module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'http://engine.gakkixmasami.com:30082/',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    },
    host: 'salamander.gakkixmasami.com',
    port: 8080,
    https: false
  }
}
