import Axios from 'axios'

const axiosDefault = Axios.create({
  baseURL: '/api'
})

function api ({ method, url, data = null, params = null, isNeedToken = false }) {
  return axiosDefault({
    url,
    method,
    headers: {
      'Api-Token': isNeedToken ? localStorage.token : ''
    },
    params,
    data
  })
}

export function Post (url, { data, params, isNeedToken = false } = {}) {
  return api({ method: 'post', url, data, params, isNeedToken })
}

export function Get (url, { data, params, isNeedToken = false } = {}) {
  return api({ method: 'get', url, data, params, isNeedToken })
}
